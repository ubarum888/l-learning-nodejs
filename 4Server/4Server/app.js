var express = require('express')
var fs = require('fs')
var bodyParser = require('body-parser')
var app = express()
var http = require('http').Server(app)
var io = require('socket.io')(http)
var mongoose = require('mongoose')

app.use(express.static(__dirname))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

mongoose.Promise = Promise

var dbUrl = "mongodb://5-01-user-1:helloyou@ds161026.mlab.com:61026/learning-nodejs-5-01";

var Message = mongoose.model('Message', {
    name: String,
    message: String
})

app.get('/messages', (req, res) =>{
    Message.find({}, (err, messages) => {
        res.send(messages);
    });
})

app.get('/messages/:user', (req, res) => {
    var user = req.params.user
    Message.find({name: user}, (err, messages) => {
        res.send(messages);
    });
})

app.post('/messages', async (req, res) => {
    try {
        throw 'error'
        if (req.body.message !== undefined && req.body.message.length !== 0) {
            var message = new Message(req.body)
            var savedMessage = message.save()
            console.log('saved')
            var censored = await Message.findOne({ message: 'badword' })
            if (censored)
                await Message.remove({ _id: censored.id })
            else
                io.emit('message', req.body)
            res.sendStatus(200)
        }
    } catch (err) {
        res.sendStatus(500)
        return console.error(err)
    } finally {
        console.log('message app.post called')
    }

    
})

io.on('connection', (socket) => {
    console.log('a user connected')
})

mongoose.connect(dbUrl, { useNewUrlParser:true }, (err) => {
    console.log('mongo db connection good', err)
})

var server = http.listen(3000, () => {
    console.log('server is listening on port', server.address().port)
})